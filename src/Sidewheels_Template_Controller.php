<?php
namespace Otomaties\WP_Sidewheels;

interface Sidewheels_Template_Controller {
	public function args();
}